import { Router } from 'express';

import { FilesRouter } from './files/files.router';

const AppRouter = Router();

AppRouter.use('/files', FilesRouter);

export { AppRouter };
