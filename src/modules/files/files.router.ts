import { Router } from 'express';

import { FilesController } from './controllers/files.controller';
import { ValidateRequestMiddleware } from '../../common/validation';
import { contentValidation, filenameValidation } from './validation/files.validation';
import { FileEnum } from './enums/file.enum';
import { CheckExtensionMiddleware } from './middlewares/check-extension.middleware';
import { RequestTarget } from '../../common/enums';

const FilesRouter = Router();

FilesRouter.get(
  '/',
  FilesController.getAll
);

FilesRouter.post(
  '/',
  ValidateRequestMiddleware([filenameValidation, contentValidation]),
  CheckExtensionMiddleware(RequestTarget.BODY),
  FilesController.create
);

FilesRouter.get(
  `/:${FileEnum.FILENAME}`,
  CheckExtensionMiddleware(RequestTarget.PARAMS),
  FilesController.getOne
);

FilesRouter.patch(
  `/:${FileEnum.FILENAME}`,
  ValidateRequestMiddleware([contentValidation]),
  CheckExtensionMiddleware(RequestTarget.PARAMS),
  FilesController.update
);

FilesRouter.delete(
  `/:${FileEnum.FILENAME}`,
  CheckExtensionMiddleware(RequestTarget.PARAMS),
  FilesController.delete
);

export { FilesRouter };
