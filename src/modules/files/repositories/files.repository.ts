import { readFile, writeFile, stat, readdir, rm } from 'fs/promises';

import { FilesRepositoryInterface } from './files.repository.interface';
import { FileInterface } from '../interfaces/file.interface';

class FilesRepositoryStatic implements FilesRepositoryInterface {
  public async getAll() {
    const path = FilesRepositoryStatic.generatePath();
    return await readdir(path);
  }

  public async findByFilename(filename: string) {
    try {
      const filepath = FilesRepositoryStatic.generatePath(filename);
      const content = await readFile(filepath, { encoding: 'utf-8' });
      const { birthtime } = await stat(filepath);
      return {
        filename,
        content,
        extension: filename.split('.').slice(-1)[0],
        uploadedDate: birthtime.toISOString()
      };
    } catch {
      return null;
    }
  }

  public async createAndSave({ filename, content }: Pick<FileInterface, 'filename' | 'content'>) {
    const filepath = FilesRepositoryStatic.generatePath(filename);
    await writeFile(filepath, content);
  }

  public async update({ filename, content }: Pick<FileInterface, 'filename' | 'content'>) {
    const filepath = FilesRepositoryStatic.generatePath(filename);
    await writeFile(filepath, content);
    return await this.findByFilename(filename);
  }

  public async delete(filename: string) {
    const filepath = FilesRepositoryStatic.generatePath(filename);
    await rm(filepath);
  }

  private static generatePath(filename?: string) {
    return filename ? `public/files/${filename}` : 'public/files/';
  }

}

export const FilesRepository = new FilesRepositoryStatic();
