import { FileInterface } from '../interfaces/file.interface';

export interface FilesRepositoryInterface {

  getAll(): Promise<string[]>;

  findByFilename(filename: string): Promise<FileInterface> | null;

  createAndSave(data: Pick<FileInterface, 'filename' | 'content'>): Promise<void>;

  update(data: Pick<FileInterface, 'filename' | 'content'>): Promise<FileInterface> | null;

  delete(filename: string): Promise<void>;

}
