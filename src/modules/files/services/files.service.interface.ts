import { FileInterface } from '../interfaces/file.interface';

export interface FilesServiceInterface {

  getAll(): Promise<{message: string, files: string[]}>;

  getOne(filename: string): Promise<{message: string} & FileInterface>;

  create(data: Pick<FileInterface, 'filename' | 'content'>): Promise<{message: string}>;

  update(data: Pick<FileInterface, 'filename' | 'content'>): Promise<{message: string} & FileInterface>;

  delete(filename: string): Promise<{message: string}>;

}
