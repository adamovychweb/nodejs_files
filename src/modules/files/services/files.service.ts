import { FilesRepository } from '../repositories/files.repository';
import { BadRequestException } from '../../../common/exceptions';
import { FilesServiceInterface } from './files.service.interface';
import { FileInterface } from '../interfaces/file.interface';

class FilesServiceStatic implements FilesServiceInterface {
  public async getAll() {
    const files = await FilesRepository.getAll();
    return {
      message: 'Success',
      files
    };
  }

  public async getOne(filename: string) {
    const res = await FilesServiceStatic.checkFileExists(filename);
    return {
      message: 'Success',
      ...res
    };
  }

  public async create({ filename, content }: Pick<FileInterface, 'filename' | 'content'>) {
    const res = await FilesRepository.findByFilename(filename);
    if (res) {
      throw new BadRequestException(`File with '${filename}' filename already exists`);
    }
    await FilesRepository.createAndSave({ filename, content });
    return {
      message: 'File created successfully'
    };
  }

  public async update({ filename, content }: Pick<FileInterface, 'filename' | 'content'>) {
    await FilesServiceStatic.checkFileExists(filename);
    const res = await FilesRepository.update({ filename, content });
    return {
      message: 'File updated successfully',
      ...res
    };
  }

  public async delete(filename: string) {
    await FilesServiceStatic.checkFileExists(filename);
    await FilesRepository.delete(filename);
    return {
      message: 'File deleted successfully'
    };
  }

  private static async checkFileExists(filename: string) {
    const res = await FilesRepository.findByFilename(filename);
    if (!res) {
      throw new BadRequestException(`No file with '${filename}' filename found`);
    }
    return res;
  }
}

export const FilesService = new FilesServiceStatic();
