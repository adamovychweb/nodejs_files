import { body } from 'express-validator';

import { GenerateValidationMessage } from '../../../common/validation';
import { FileEnum } from '../enums/file.enum';

export const filenameValidation = body(FileEnum.FILENAME)
  .notEmpty().withMessage(GenerateValidationMessage.notEmpty(FileEnum.FILENAME))
  .isString().withMessage(GenerateValidationMessage.isString(FileEnum.FILENAME))
;

export const contentValidation = body(FileEnum.CONTENT)
  .notEmpty().withMessage(GenerateValidationMessage.notEmpty(FileEnum.CONTENT))
  .isString().withMessage(GenerateValidationMessage.isString(FileEnum.CONTENT))
;
