import { NextFunction, Request, Response } from 'express';

import { RequestTarget } from '../../../common/enums';
import { BadRequestException } from '../../../common/exceptions';

export function CheckExtensionMiddleware(reqTarget: RequestTarget) {
  return function (req: Request, res: Response, next: NextFunction) {
    const { filename } = req[reqTarget];
    const [extension] = (filename as string).split('.').slice(-1);
    const validExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
    if (!validExtensions.includes(extension)) {
      return next(new BadRequestException('Wrong extension'));
    }
    next();
  };
}
