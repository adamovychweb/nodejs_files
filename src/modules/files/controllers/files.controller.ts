import { NextFunction, Request, Response } from 'express';

import { HttpStatusCodes } from '../../../common/enums';
import { FilesService } from '../services/files.service';

class FilesControllerStatic {
  public async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      const response = await FilesService.getAll();

      return res
        .status(HttpStatusCodes.OK)
        .json(response);
    } catch (e) {
      next(e);
    }
  }

  public async getOne(req: Request, res: Response, next: NextFunction) {
    try {
      const { filename } = req.params;
      const response = await FilesService.getOne(filename);

      return res
        .status(HttpStatusCodes.OK)
        .json(response);
    } catch (e) {
      next(e);
    }
  }

  public async create(req: Request, res: Response, next: NextFunction) {
    try {
      const { filename, content } = req.body;
      const response = await FilesService.create({ filename, content });

      return res
        .status(HttpStatusCodes.OK)
        .json(response);
    } catch (e) {
      next(e);
    }
  }

  public async update(req: Request, res: Response, next: NextFunction) {
    try {
      const { filename } = req.params;
      const { content } = req.body;
      const response = await FilesService.update({ filename, content });

      return res
        .status(HttpStatusCodes.OK)
        .json(response);
    } catch (e) {
      next(e);
    }
  }

  public async delete(req: Request, res: Response, next: NextFunction) {
    try {
      const { filename } = req.params;
      const response = await FilesService.delete(filename);

      return res
        .status(HttpStatusCodes.OK)
        .json(response);
    } catch (e) {
      next(e);
    }
  }

}

export const FilesController = new FilesControllerStatic();
