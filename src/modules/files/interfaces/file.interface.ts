export interface FileInterface {
  filename: string;
  content: string;
  extension: string;
  uploadedDate: string;
}
