class GenerateValidationMessageStatic {
  public notEmpty(param: string) {
    return `Please specify '${param}' parameter`;
  }

  public isString(param: string) {
    return `Parameter '${param}' must be string`;
  }
}

export const GenerateValidationMessage = new GenerateValidationMessageStatic();
