import { Request, Response, NextFunction } from 'express';
import { ValidationChain, validationResult } from 'express-validator';
import { BadRequestException } from '../exceptions';

export function ValidateRequestMiddleware(validationArray: ValidationChain[]) {
  return async function (req: Request, res: Response, next: NextFunction) {
    for (let validation of validationArray) {
      await validation.run(req);
    }
    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }
    console.log('ValidateRequestMiddleware: ', errors.array());
    const errorsArray = errors.array().map(err => err.msg);
    return next(new BadRequestException(errorsArray[0]));
  };
}
