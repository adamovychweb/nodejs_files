export class HttpException extends Error {
  private readonly _statusCode;
  private readonly _response;

  constructor(
    statusCode: number,
    message: string
  ) {
    super();
    this._statusCode = statusCode;
    this._response = {
      message
    };
  }

  public get statusCode(): number {
    return this._statusCode;
  }

  public get response(): string {
    return this._response;
  }
}