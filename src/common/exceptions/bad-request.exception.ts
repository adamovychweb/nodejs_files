import { HttpException } from './http.exception';
import { HttpStatusCodes } from '../enums';

export class BadRequestException extends HttpException {
  constructor(
    message: string
  ) {
    super(HttpStatusCodes.BAD_REQUEST, message);
  }
}