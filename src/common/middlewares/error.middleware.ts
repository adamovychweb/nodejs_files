import { NextFunction, Request, Response } from 'express';

import { HttpException } from '../exceptions';
import { HttpStatusCodes } from '../enums';

export function ErrorMiddleware(err: Error, req: Request, res: Response, next: NextFunction) {
  if (err instanceof HttpException) {
    return res
      .status(err.statusCode)
      .json(err.response);
  }

  res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Server error' });
}
