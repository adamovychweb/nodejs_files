export enum RequestTarget {
  BODY = 'body',
  PARAMS = 'params'
}