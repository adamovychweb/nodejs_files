import express, { Application } from 'express';
import cors from 'cors';

import { AppRouter } from './modules/app-router';
import { ServerSettings } from './common/enums';
import { ErrorMiddleware } from './common/middlewares';

const app: Application = express();
app.use(express.json());
app.use(cors());

app.use('/api', AppRouter);
app.use(ErrorMiddleware);

function startServer() {
  try {
    app.listen(ServerSettings.PORT, () => console.log(`Server running on port: ${ServerSettings.PORT}`));
  } catch (e) {
    console.log(e);
  }
}

startServer();
