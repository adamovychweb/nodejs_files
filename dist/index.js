"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const app_router_1 = require("./modules/app-router");
const enums_1 = require("./common/enums");
const middlewares_1 = require("./common/middlewares");
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use((0, cors_1.default)());
app.use('/api', app_router_1.AppRouter);
app.use(middlewares_1.ErrorMiddleware);
function startServer() {
    try {
        app.listen(enums_1.ServerSettings.PORT, () => console.log(`Server running on port: ${enums_1.ServerSettings.PORT}`));
    }
    catch (e) {
        console.log(e);
    }
}
startServer();
//# sourceMappingURL=index.js.map