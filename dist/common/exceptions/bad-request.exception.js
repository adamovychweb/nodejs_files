"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BadRequestException = void 0;
const http_exception_1 = require("./http.exception");
const enums_1 = require("../enums");
class BadRequestException extends http_exception_1.HttpException {
    constructor(message) {
        super(enums_1.HttpStatusCodes.BAD_REQUEST, message);
    }
}
exports.BadRequestException = BadRequestException;
//# sourceMappingURL=bad-request.exception.js.map