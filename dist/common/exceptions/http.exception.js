"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpException = void 0;
class HttpException extends Error {
    constructor(statusCode, message) {
        super();
        this._statusCode = statusCode;
        this._response = {
            message
        };
    }
    get statusCode() {
        return this._statusCode;
    }
    get response() {
        return this._response;
    }
}
exports.HttpException = HttpException;
//# sourceMappingURL=http.exception.js.map