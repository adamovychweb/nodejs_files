"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorMiddleware = void 0;
const exceptions_1 = require("../exceptions");
const enums_1 = require("../enums");
function ErrorMiddleware(err, req, res, next) {
    if (err instanceof exceptions_1.HttpException) {
        return res
            .status(err.statusCode)
            .json(err.response);
    }
    res.status(enums_1.HttpStatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Server error' });
}
exports.ErrorMiddleware = ErrorMiddleware;
//# sourceMappingURL=error.middleware.js.map