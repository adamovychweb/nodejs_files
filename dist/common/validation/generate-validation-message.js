"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateValidationMessage = void 0;
class GenerateValidationMessageStatic {
    notEmpty(param) {
        return `Please specify '${param}' parameter`;
    }
    isString(param) {
        return `Parameter '${param}' must be string`;
    }
}
exports.GenerateValidationMessage = new GenerateValidationMessageStatic();
//# sourceMappingURL=generate-validation-message.js.map