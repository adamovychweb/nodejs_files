"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestTarget = void 0;
var RequestTarget;
(function (RequestTarget) {
    RequestTarget["BODY"] = "body";
    RequestTarget["PARAMS"] = "params";
})(RequestTarget = exports.RequestTarget || (exports.RequestTarget = {}));
//# sourceMappingURL=request-target.js.map