"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppRouter = void 0;
const express_1 = require("express");
const files_router_1 = require("./files/files.router");
const AppRouter = (0, express_1.Router)();
exports.AppRouter = AppRouter;
AppRouter.use('/files', files_router_1.FilesRouter);
//# sourceMappingURL=app-router.js.map