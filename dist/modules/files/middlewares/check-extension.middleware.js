"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckExtensionMiddleware = void 0;
const exceptions_1 = require("../../../common/exceptions");
function CheckExtensionMiddleware(reqTarget) {
    return function (req, res, next) {
        const { filename } = req[reqTarget];
        const [extension] = filename.split('.').slice(-1);
        const validExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
        if (!validExtensions.includes(extension)) {
            return next(new exceptions_1.BadRequestException('Wrong extension'));
        }
        next();
    };
}
exports.CheckExtensionMiddleware = CheckExtensionMiddleware;
//# sourceMappingURL=check-extension.middleware.js.map