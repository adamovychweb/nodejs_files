"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilesRouter = void 0;
const express_1 = require("express");
const files_controller_1 = require("./controllers/files.controller");
const validation_1 = require("../../common/validation");
const files_validation_1 = require("./validation/files.validation");
const file_enum_1 = require("./enums/file.enum");
const check_extension_middleware_1 = require("./middlewares/check-extension.middleware");
const enums_1 = require("../../common/enums");
const FilesRouter = (0, express_1.Router)();
exports.FilesRouter = FilesRouter;
FilesRouter.get('/', files_controller_1.FilesController.getAll);
FilesRouter.post('/', (0, validation_1.ValidateRequestMiddleware)([files_validation_1.filenameValidation, files_validation_1.contentValidation]), (0, check_extension_middleware_1.CheckExtensionMiddleware)(enums_1.RequestTarget.BODY), files_controller_1.FilesController.create);
FilesRouter.get(`/:${file_enum_1.FileEnum.FILENAME}`, (0, check_extension_middleware_1.CheckExtensionMiddleware)(enums_1.RequestTarget.PARAMS), files_controller_1.FilesController.getOne);
FilesRouter.patch(`/:${file_enum_1.FileEnum.FILENAME}`, (0, validation_1.ValidateRequestMiddleware)([files_validation_1.contentValidation]), (0, check_extension_middleware_1.CheckExtensionMiddleware)(enums_1.RequestTarget.PARAMS), files_controller_1.FilesController.update);
FilesRouter.delete(`/:${file_enum_1.FileEnum.FILENAME}`, (0, check_extension_middleware_1.CheckExtensionMiddleware)(enums_1.RequestTarget.PARAMS), files_controller_1.FilesController.delete);
//# sourceMappingURL=files.router.js.map