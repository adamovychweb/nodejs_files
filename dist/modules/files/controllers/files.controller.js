"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilesController = void 0;
const enums_1 = require("../../../common/enums");
const files_service_1 = require("../services/files.service");
class FilesControllerStatic {
    getAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield files_service_1.FilesService.getAll();
                return res
                    .status(enums_1.HttpStatusCodes.OK)
                    .json(response);
            }
            catch (e) {
                next(e);
            }
        });
    }
    getOne(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { filename } = req.params;
                const response = yield files_service_1.FilesService.getOne(filename);
                return res
                    .status(enums_1.HttpStatusCodes.OK)
                    .json(response);
            }
            catch (e) {
                next(e);
            }
        });
    }
    create(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { filename, content } = req.body;
                const response = yield files_service_1.FilesService.create({ filename, content });
                return res
                    .status(enums_1.HttpStatusCodes.OK)
                    .json(response);
            }
            catch (e) {
                next(e);
            }
        });
    }
    update(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { filename } = req.params;
                const { content } = req.body;
                const response = yield files_service_1.FilesService.update({ filename, content });
                return res
                    .status(enums_1.HttpStatusCodes.OK)
                    .json(response);
            }
            catch (e) {
                next(e);
            }
        });
    }
    delete(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { filename } = req.params;
                const response = yield files_service_1.FilesService.delete(filename);
                return res
                    .status(enums_1.HttpStatusCodes.OK)
                    .json(response);
            }
            catch (e) {
                next(e);
            }
        });
    }
}
exports.FilesController = new FilesControllerStatic();
//# sourceMappingURL=files.controller.js.map