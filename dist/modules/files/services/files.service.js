"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilesService = void 0;
const files_repository_1 = require("../repositories/files.repository");
const exceptions_1 = require("../../../common/exceptions");
class FilesServiceStatic {
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const files = yield files_repository_1.FilesRepository.getAll();
            return {
                message: 'Success',
                files
            };
        });
    }
    getOne(filename) {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield FilesServiceStatic.checkFileExists(filename);
            return Object.assign({ message: 'Success' }, res);
        });
    }
    create({ filename, content }) {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield files_repository_1.FilesRepository.findByFilename(filename);
            if (res) {
                throw new exceptions_1.BadRequestException(`File with '${filename}' filename already exists`);
            }
            yield files_repository_1.FilesRepository.createAndSave({ filename, content });
            return {
                message: 'File created successfully'
            };
        });
    }
    update({ filename, content }) {
        return __awaiter(this, void 0, void 0, function* () {
            yield FilesServiceStatic.checkFileExists(filename);
            const res = yield files_repository_1.FilesRepository.update({ filename, content });
            return Object.assign({ message: 'File updated successfully' }, res);
        });
    }
    delete(filename) {
        return __awaiter(this, void 0, void 0, function* () {
            yield FilesServiceStatic.checkFileExists(filename);
            yield files_repository_1.FilesRepository.delete(filename);
            return {
                message: 'File deleted successfully'
            };
        });
    }
    static checkFileExists(filename) {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield files_repository_1.FilesRepository.findByFilename(filename);
            if (!res) {
                throw new exceptions_1.BadRequestException(`No file with '${filename}' filename found`);
            }
            return res;
        });
    }
}
exports.FilesService = new FilesServiceStatic();
//# sourceMappingURL=files.service.js.map