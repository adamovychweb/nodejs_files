"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilesRepository = void 0;
const promises_1 = require("fs/promises");
class FilesRepositoryStatic {
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const path = FilesRepositoryStatic.generatePath();
            return yield (0, promises_1.readdir)(path);
        });
    }
    findByFilename(filename) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const filepath = FilesRepositoryStatic.generatePath(filename);
                const content = yield (0, promises_1.readFile)(filepath, { encoding: 'utf-8' });
                const { birthtime } = yield (0, promises_1.stat)(filepath);
                return {
                    filename,
                    content,
                    extension: filename.split('.').slice(-1)[0],
                    uploadedDate: birthtime.toISOString()
                };
            }
            catch (_a) {
                return null;
            }
        });
    }
    createAndSave({ filename, content }) {
        return __awaiter(this, void 0, void 0, function* () {
            const filepath = FilesRepositoryStatic.generatePath(filename);
            yield (0, promises_1.writeFile)(filepath, content);
        });
    }
    update({ filename, content }) {
        return __awaiter(this, void 0, void 0, function* () {
            const filepath = FilesRepositoryStatic.generatePath(filename);
            yield (0, promises_1.writeFile)(filepath, content);
            return yield this.findByFilename(filename);
        });
    }
    delete(filename) {
        return __awaiter(this, void 0, void 0, function* () {
            const filepath = FilesRepositoryStatic.generatePath(filename);
            yield (0, promises_1.rm)(filepath);
        });
    }
    static generatePath(filename) {
        return filename ? `public/files/${filename}` : 'public/files/';
    }
}
exports.FilesRepository = new FilesRepositoryStatic();
//# sourceMappingURL=files.repository.js.map