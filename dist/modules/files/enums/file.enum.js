"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileEnum = void 0;
var FileEnum;
(function (FileEnum) {
    FileEnum["FILENAME"] = "filename";
    FileEnum["CONTENT"] = "content";
})(FileEnum = exports.FileEnum || (exports.FileEnum = {}));
//# sourceMappingURL=file.enum.js.map