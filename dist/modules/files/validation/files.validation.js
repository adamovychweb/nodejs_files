"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.contentValidation = exports.filenameValidation = void 0;
const express_validator_1 = require("express-validator");
const validation_1 = require("../../../common/validation");
const file_enum_1 = require("../enums/file.enum");
exports.filenameValidation = (0, express_validator_1.body)(file_enum_1.FileEnum.FILENAME)
    .notEmpty().withMessage(validation_1.GenerateValidationMessage.notEmpty(file_enum_1.FileEnum.FILENAME))
    .isString().withMessage(validation_1.GenerateValidationMessage.isString(file_enum_1.FileEnum.FILENAME));
exports.contentValidation = (0, express_validator_1.body)(file_enum_1.FileEnum.CONTENT)
    .notEmpty().withMessage(validation_1.GenerateValidationMessage.notEmpty(file_enum_1.FileEnum.CONTENT))
    .isString().withMessage(validation_1.GenerateValidationMessage.isString(file_enum_1.FileEnum.CONTENT));
//# sourceMappingURL=files.validation.js.map